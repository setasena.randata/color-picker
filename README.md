# Color Picker

** Version 1.0.0 **

A color picker program that allows you to save all of your projects color palettes and with a single click, you can get the hex color code.

---

## Latar Belakang ##

Pembuatan program ini bertujuan untuk memudahkan pengguna untuk menyimpan sekaligus copy to clipboard hex color code project mereka. Sangat melelahkan rasanya jika kita perlu memindahkan satu satu color code dari sebuah aplikasi ke aplikasi lain. Sebagai contoh, membuat color palette di aplikasi Figma memang sangat mudah untuk digunakan kembali di project figma apapun. Namun, sulit untuk kita semua memindahkan color palette dari Figma ke aplikasi lain, contohnya, Adobe Illustrator, Adobe Photoshop, dan lain-lain. Tidak hanya itu, color palette yang telah di register oleh user, akan disimpan pada sebuah txt file bernama "Storage.txt" sehingga kedepannya jika Color Picker dibuka, Color picker akan secara otomatis membaca apa saja color palette yang pernah disimpan.

## Demo Program ##

Untuk demo program Color Picker, saya membuat video dan menguploadnya ke google drive.
https://drive.google.com/file/d/1iasMVmszvb3l5Zq_HG42K7iZ1sYcAwy0/view?usp=sharing

## License & copyright ##

© Setasena Randata Ramadanie, Fakultas Ilmu Komputer Universitas Indonesia