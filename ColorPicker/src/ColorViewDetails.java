import java.util.ArrayList;
import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;

public class ColorViewDetails {

    private JLabel titleLabel;
    private ColorPalette theChosenOne = null;
    private JPanel panelHome;
    private String[] daftarWarna = null;
    private JButton backButton;
    private JLabel descriptionLabel;

    public ColorViewDetails(JFrame frame, String nama, ArrayList<ColorPalette> daftarColorPalette) {
        frame.getContentPane().removeAll();
        
        for (ColorPalette colorPalette : daftarColorPalette) {
            if (colorPalette.getNama().equals(nama)) {
                theChosenOne = colorPalette;
                daftarWarna = theChosenOne.getDaftarWarna();
            }
        }
        JButton[] jb = new JButton[theChosenOne.getTotalColors()];

        titleLabel = new JLabel();
        titleLabel.setText(nama + " Color Palettes");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(AppGUI.fontTitle);

        descriptionLabel = new JLabel();
        descriptionLabel.setText("Click to copy to your clipboard");
        descriptionLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        descriptionLabel.setFont(AppGUI.fontGeneral);

        backButton = new JButton("Done");
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(new Color(64, 160, 149));
        backButton.setForeground(Color.WHITE);
        backButton.setBorderPainted(false);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                frame.setVisible(false);
                try {
                    new HomeGUI(frame, daftarColorPalette);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });

        for (int i = 0; i < theChosenOne.getTotalColors(); i++) {

            if (daftarWarna[i] != null) {

                String warna = daftarWarna[i];
    
                int x = Integer.valueOf(warna.substring(1, 3), 16);
                int y = Integer.valueOf(warna.substring(3, 5), 16);
                int z = Integer.valueOf(warna.substring(5, 7), 16);
    
                jb[i] = new JButton(warna);
                jb[i].setAlignmentX(Component.CENTER_ALIGNMENT);
                jb[i].setBackground(new Color(x, y, z));
                jb[i].setForeground(Color.WHITE);
                jb[i].setBorderPainted(false);
                jb[i].addActionListener(new ActionListener() {
                    public void actionPerformed (ActionEvent e) {
                        StringSelection stringSelect = new StringSelection(warna);
                        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
                        clip.setContents(stringSelect, null);
                        JOptionPane.showMessageDialog(frame, "Hex color copied to clipboard");
                    }
                });

            }
        }

        panelHome = new JPanel();

        Box box = Box.createVerticalBox();

        box.add(titleLabel);
        box.add(Box.createRigidArea(new Dimension (0, 12)));
        box.add(descriptionLabel);
        box.add(Box.createRigidArea(new Dimension (0, 20)));
        for (int i = 0; i < theChosenOne.getTotalColors(); i++) {
            box.add(jb[i]);
            box.add(Box.createRigidArea(new Dimension (0, 12)));
        }
        box.add(Box.createRigidArea(new Dimension (0, 12)));
        box.add(backButton);

        panelHome.add(box);
        frame.add(panelHome);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);




    }
}
