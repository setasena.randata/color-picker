import java.io.FileNotFoundException;
import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
       new AppGUI();
    }
}

class AppGUI extends JFrame {
    
    public static Font fontGeneral = new Font("Inter", Font.PLAIN, 14);
    public static Font fontTitle = new Font("Inter", Font.BOLD, 20);
    private static ArrayList<ColorPalette> daftarColorPalette = new ArrayList<ColorPalette>();
    
    public AppGUI() throws FileNotFoundException {
        JFrame frame = new JFrame();
        frame.setTitle("Color Picker - Setasena Randata Ramadanie");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(new GridBagLayout());

        new HomeGUI(frame, daftarColorPalette);
        frame.setVisible(true);
    }
}
