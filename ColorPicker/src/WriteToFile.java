import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {
    public static void writeNow(String x) {
        try {
            FileWriter myWriter = new FileWriter("Storage.txt", true);
            BufferedWriter out = new BufferedWriter(myWriter);
            out.write(x);
            out.newLine();
            out.close();
            System.out.println("Successfully wrote to the file.");
          } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
    }
}
