import java.util.ArrayList;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;  

public class ColorView {
    
    private JLabel titleLabel;
    private JPanel panelHome;
    private JButton backButton;

    public ColorView(JFrame frame, ArrayList<String> projectList, ArrayList<ColorPalette> daftarColorPalette) {
        frame.getContentPane().removeAll();
        JButton[] jb = new JButton[projectList.size()];

        titleLabel = new JLabel();
        titleLabel.setText("Select your project");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(AppGUI.fontTitle);

        backButton = new JButton("Back");
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(new Color(64, 160, 149));
        backButton.setForeground(Color.WHITE);
        backButton.setBorderPainted(false);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                frame.setVisible(false);
                try {
                    new HomeGUI(frame, daftarColorPalette);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });

        panelHome = new JPanel();

        int jumlah = projectList.size();

        for (int i = 0; i < jumlah; i++) {
            jb[i] = new JButton(projectList.get(i));
            jb[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            jb[i].setBackground(new Color(64, 160, 149));
            jb[i].setForeground(Color.WHITE);
            jb[i].setBorderPainted(false);
            String nama = projectList.get(i);
            jb[i].addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                frame.setVisible(false);
                new ColorViewDetails(frame, nama, daftarColorPalette);
            }
        });
        }

        Box box = Box.createVerticalBox();
        box.add(titleLabel);
        box.add(Box.createRigidArea(new Dimension (0, 20)));
        for (int i = 0; i < jumlah; i++) {
            box.add(jb[i]);
            box.add(Box.createRigidArea(new Dimension (0, 12)));
        }
        box.add(Box.createRigidArea(new Dimension (0, 12)));
        box.add(backButton);
        panelHome.add(box);
        frame.add(panelHome);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
}
