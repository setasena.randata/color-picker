import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class StoreColorGUI {

    private JLabel titleLabel;
    private JLabel descriptionLabel;
    private JPanel panelHome;
    private JButton registerButton;

    public StoreColorGUI(JFrame frame, String projectName, int amount, ArrayList<ColorPalette> daftarColorPalette) {
        frame.getContentPane().removeAll();

        JTextField[] jtf = new JTextField[amount];

        titleLabel = new JLabel();
        titleLabel.setText(projectName);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(AppGUI.fontTitle);
        
        descriptionLabel = new JLabel();
        descriptionLabel.setText("Input your hex color code in the fields below");
        descriptionLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        descriptionLabel.setFont(AppGUI.fontGeneral);

        registerButton = new JButton("Register");
        registerButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        registerButton.setBackground(new Color(64, 160, 149));
        registerButton.setForeground(Color.WHITE);
        registerButton.setBorderPainted(false);
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                Boolean valid = true;

                for (int i = 0; i < amount; i++) {
                    if (jtf[i].getText().equals("")) {
                        valid = false;
                    }
                }

                if (valid == false) {
                    JOptionPane.showMessageDialog(frame, "Please complete all fields");
                } else {
                    String x = "\n" + "$" + amount + projectName;
                    for (int i = 0; i < amount; i++) {
                        x += "\n" + jtf[i].getText();
                    }
                    WriteToFile.writeNow(x);
                    JOptionPane.showMessageDialog(frame, "Succes!");
                    frame.setVisible(false);

                    try {
                        new HomeGUI(frame, daftarColorPalette);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        panelHome = new JPanel();

        for (int i=0; i < amount; i++) {
            jtf[i] = new JTextField();
        }

        Box box = Box.createVerticalBox();
        
        box.add(titleLabel);
        box.add(Box.createRigidArea(new Dimension(0, 20)));
        box.add(descriptionLabel);
        box.add(Box.createRigidArea(new Dimension(0, 12)));
        for (int i = 0; i < amount; i++) {
            box.add(jtf[i]);
            box.add(Box.createRigidArea(new Dimension(0, 12)));
        }
        box.add(registerButton);

        panelHome.add(box);
        frame.add(panelHome);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);

    }
}
