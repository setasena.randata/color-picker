import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.io.*;  
import java.util.Scanner;  

public class HomeGUI {

    private JLabel titleLabel;
    private JLabel descriptionLabel;
    private JLabel createdByLabel;
    private JLabel creatorLabel;
    private JButton registerButton;
    private JButton pickButton;
    private JButton exitButton;
    private JPanel panelHome;

    public HomeGUI(JFrame frame, ArrayList<ColorPalette> daftarColorPalette) throws FileNotFoundException {
        readColor(daftarColorPalette);
        frame.getContentPane().removeAll();

        titleLabel = new JLabel();
        titleLabel.setText("Welcome to Color Picker!");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(AppGUI.fontTitle);

        descriptionLabel = new JLabel();
        descriptionLabel.setText("Register your color now or pick it right away!");
        descriptionLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        descriptionLabel.setFont(AppGUI.fontGeneral);

        createdByLabel = new JLabel();
        createdByLabel.setText("created by");
        createdByLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        createdByLabel.setFont(AppGUI.fontGeneral);

        creatorLabel = new JLabel();
        creatorLabel.setText("Setasena Randata Ramadanie");
        creatorLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        creatorLabel.setFont(AppGUI.fontGeneral);

        registerButton = new JButton("Register New Color");
        registerButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        registerButton.setBackground(new Color(64, 160, 149));
        registerButton.setForeground(Color.WHITE);
        registerButton.setBorderPainted(false);
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                frame.setVisible(false);
                new RegisterColorGUI(frame, daftarColorPalette);
            }
        });

        pickButton = new JButton("Pick Colors!");
        pickButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        pickButton.setBackground(new Color(64, 160, 149));
        pickButton.setForeground(Color.WHITE);
        pickButton.setBorderPainted(false);
        pickButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                frame.setVisible(false);
                new ColorView(frame, ColorPalette.getProjectList(), daftarColorPalette);
            }
        });

        exitButton = new JButton("Exit");
        exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitButton.setBackground(new Color(64, 160, 149));
        exitButton.setForeground(Color.WHITE);
        exitButton.setBorderPainted(false);
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                if (JOptionPane.showConfirmDialog( frame,"Confirm if you want to exit","Color Picker",
                    JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
                    System.exit(0);
            }
        });

        panelHome = new JPanel();

        Box box = Box.createVerticalBox();

        box.add(titleLabel);
        box.add(Box.createRigidArea(new  Dimension (0, 20)));
        box.add(descriptionLabel);
        box.add(Box.createRigidArea(new  Dimension (0, 12)));
        box.add(registerButton);
        box.add(Box.createRigidArea(new  Dimension (0, 12)));
        box.add(pickButton);
        box.add(Box.createRigidArea(new  Dimension (0, 12)));
        box.add(exitButton);
        box.add(Box.createRigidArea(new  Dimension (0, 24)));
        box.add(createdByLabel);
        box.add(creatorLabel);

        panelHome.add(box);
        frame.add(panelHome);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
    
    public void readColor(ArrayList<ColorPalette> daftarColorPalette) throws FileNotFoundException {
        ColorPalette.resetAll();

        try {

            FileInputStream fis = new FileInputStream("Storage.txt");
            Scanner sc = new Scanner(fis);
            
            while (sc.hasNextLine()) {
                String data = sc.nextLine();
                if (data.length() != 0 && data.substring(0,1).equals("$")) {
                    String projectName = data.substring(2);
                    int amount = Integer.parseInt(data.substring(1,2));
                    ColorPalette colPal = new ColorPalette(projectName, amount);
                    daftarColorPalette.add(colPal);
                    for (int i = 0; i < amount; i++) {
                        colPal.addColorPalette(sc.nextLine());
                    }
                }
            }
            sc.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
