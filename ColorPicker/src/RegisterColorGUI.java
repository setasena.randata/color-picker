import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class RegisterColorGUI {

    private JLabel titleLabel;
    private JLabel descriptionLabel;
    private JLabel nameLabel;
    private JLabel amountLabel;
    private JTextField nameField;
    private JTextField amountField;
    private JButton nextButton;
    private JButton backButton;
    private JPanel panelHome;
    private int amount;
    private String projectName;

    public RegisterColorGUI(JFrame frame, ArrayList<ColorPalette> daftarColorPalette) {
        frame.getContentPane().removeAll();

        titleLabel = new JLabel();
        titleLabel.setText("Type your Project Name");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(AppGUI.fontTitle);

        descriptionLabel = new JLabel();
        descriptionLabel.setText("And type how many colors you want to register");
        descriptionLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        descriptionLabel.setFont(AppGUI.fontGeneral);

        nameLabel = new JLabel();
        nameLabel.setText("Enter your project name");
        nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        nameLabel.setFont(AppGUI.fontGeneral);

        amountLabel = new JLabel();
        amountLabel.setText("Enter the amount of colors you want to register");
        amountLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        amountLabel.setFont(AppGUI.fontGeneral);

        nameField = new JTextField();
        nameField.setAlignmentX(Component.CENTER_ALIGNMENT);

        amountField = new JTextField();
        amountField.setAlignmentX(Component.CENTER_ALIGNMENT);

        nextButton = new JButton("Next");
        nextButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        nextButton.setBackground(new Color(64, 160, 149));
        nextButton.setForeground(Color.WHITE);
        nextButton.setBorderPainted(false);
        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                if (nameField.getText().equals("") || amountField.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Please complete all fields");
                } else {
                    amount = Integer.parseInt(amountField.getText());
                    projectName = nameField.getText();
                    frame.setVisible(false);
                    new StoreColorGUI(frame, projectName, amount, daftarColorPalette);
                }
            }
        });

        backButton = new JButton("Back");
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(new Color(64, 160, 149));
        backButton.setForeground(Color.WHITE);
        backButton.setBorderPainted(false);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                frame.setVisible(false);
                try {
                    new HomeGUI(frame, daftarColorPalette);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });

        panelHome = new JPanel();

        Box box = Box.createVerticalBox();

        box.add(titleLabel);
        box.add(Box.createRigidArea(new Dimension(0, 20)));
        box.add(descriptionLabel);
        box.add(Box.createRigidArea(new Dimension(0, 12)));
        box.add(nameLabel);
        box.add(Box.createRigidArea(new Dimension(0, 12)));
        box.add(nameField);
        box.add(Box.createRigidArea(new Dimension(0, 12)));
        box.add(amountLabel);
        box.add(Box.createRigidArea(new Dimension(0, 12)));
        box.add(amountField);
        box.add(Box.createRigidArea(new Dimension(0, 20)));
        box.add(nextButton);
        box.add(Box.createRigidArea(new Dimension(0, 12)));
        box.add(backButton);

        panelHome.add(box);
        frame.add(panelHome);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
}
