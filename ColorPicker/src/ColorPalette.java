import java.util.ArrayList;

public class ColorPalette {
    private String projectName;
    private int amount;
    private static ArrayList<String> projectList = new ArrayList<String>();
    String[] daftarWarna = new String[10];

    public ColorPalette(String projectName, int amount) {
        this.projectName = projectName;
        this.amount = amount;
        projectList.add(projectName);
    }

    public void addColorPalette(String color) {
        int i = 0;
        boolean valid = true;
        for (String string : daftarWarna) {
            if (string != null && string.equals(color)) valid = false;
        }
        if (valid == true) {
            while (daftarWarna[i] != null) i++;
            daftarWarna[i] = color;
        }
    }

    public void addProjectList() {
        projectList.add(projectName);
    }

    public String[] getDaftarWarna() {
        return daftarWarna;
    }

    public String getNama() {
        return this.projectName;
    }

    public int getTotalColors() {
        return this.amount;
    }

    public static int getTotalPalette() {
        return projectList.size();
    }

    public static ArrayList<String> getProjectList() {
        return projectList;
    }
    
    public static void resetAll() {
        projectList.clear();
    }
}
